<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	
	class DiffTest extends TestCase {
		public function testEquality() {
			$this->assertSame(
				[1, 2, 3, 4, 5, 6],
				[1, 4, 2, 3, 5, 6]
			);
		}
	 }	
?>

