<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	
	class NimTest extends TestCase {
		public function testEquality() {
			$this->assertSame (
				[3, 4, 1, 1, 1, 5, 1, 1, 6, 2],
				[3, 4, 1, 1, 1, 5, 1, 1, 2, 7]
			);
		}
	}	
?>